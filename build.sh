#!/bin/bash

set -e

export INSTALL="$(pwd)/install"
export DOWNLOADS="$(pwd)/downloads"
export DEB_VERSION=1.2.20191219

mkdir -p $INSTALL
mkdir -p $DOWNLOADS
rm -rf $INSTALL/*

[ -f $DOWNLOADS/mricrogl-$DEB_VERSION.zip ] || wget -O  $DOWNLOADS/mricrogl-$DEB_VERSION.zip "https://github.com/rordenlab/MRIcroGL12/releases/download/v$DEB_VERSION/MRIcroGL_linux.zip"

cd $INSTALL
mkdir -p opt
cd opt
unzip $DOWNLOADS/mricrogl-$DEB_VERSION.zip

cd $INSTALL
mkdir -p usr/share/applications
cat <<EOF > usr/share/applications/MRIcroGL.desktop
[Desktop Entry]
Type=Application
Name=MRIcroGL
Icon=/opt/MRIcroGL/Resources/mricrogl.svg
Exec=env MESA_GL_VERSION_OVERRIDE=3.3FC /opt/MRIcroGL/MRIcroGL
Terminal=false
Categories=Utility;Science;MedicalSoftware;
EOF

cd ..

fpm -s dir -t deb -n mricrogl -v $DEB_VERSION \
   --description="MRIcroGL - display 3D medical imaging" \
   --after-install after-install.sh \
   --before-remove before-remove.sh \
  -C $INSTALL opt/ usr/ 
